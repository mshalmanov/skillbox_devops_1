FROM node

RUN mkdir /app

#Set default dir
WORKDIR /app

COPY package.json /app
RUN yarn install

COPY . /app

#RUN yarn test
RUN yarn build

#run app on the port 3000 
EXPOSE 3000

CMD yarn start




